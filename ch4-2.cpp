#include<iostream>
using namespace std;
const int MAXSIZE = 1000;
typedef char datatype ;
struct Linklist{
	datatype data;
	Linklist *next;
};
typedef Linklist* datatype2 ;
struct Stack{
	datatype2 data[MAXSIZE];
	int top;
};
Stack *new_Stack(){
	Stack *s=new Stack;
	s->top=0;
	return s ;
}
Linklist *new_Linklist(){
	Linklist *head = new Linklist;
	Linklist *tail = head,*p;
	datatype data;
	int i=0;
	while (cin.get(data)){
		if (data!='\n'){
			p=new Linklist;
			p->data=data;
			tail->next=p;
			tail=p;
		}else{
			tail->next = NULL;
			return head;
		}
	}
}
void print_Linkist(Linklist *head){
	Linklist *p=head->next;
	while (p!=NULL){
		cout<<p->data;
		p=p->next;
	}
	cout<<endl;
}
void push(Stack *s,datatype2 data){
	s->data[(s->top)++]=data;
}
datatype2 pop(Stack *s){
	return s->data[--(s->top)];
}
void reverse(Linklist *head,Stack *s){
	Linklist *p=head->next,*temp;
	push(s,NULL);
	while (p!=NULL){
		temp=pop(s);
		push(s,p);
		push(s,p->next);
		p->next=temp;
		p=pop(s);
	}
	head->next=pop(s);
}
int main(){
	Stack *s=new_Stack();
	Linklist *head=new_Linklist();
	print_Linkist(head);
	reverse(head,s);
	print_Linkist(head);
	return 0;
}
#include<iostream>
#include<windows.h>
#define datatype char
using namespace std;
typedef struct linklist{
       struct linklist * next;
       datatype data;
}linklist;
void reverse1(datatype *a,int n){
     for (int i=0;i<=(n/2-1);i++){
         int temp=a[n-i-1];
         a[n-i-1]=a[i];
         a[i]=temp;
     }
}
linklist * new_linklist(){
         char ch;
         linklist *head=(linklist*)malloc(sizeof(linklist));
         linklist *tail=head;
         linklist *p;
         head->next=NULL;
         while ((ch = getchar())!='\n'){
               p=(linklist*)malloc(sizeof(linklist));
               tail->next=p;
               p->data=ch;
               tail=p;
         }
         tail->next=NULL;
         return head;
}
void print_linklist(linklist *head){
     linklist *p=head->next;
     while (p != NULL){
           cout<<p->data;
           p=p->next;
     }
     cout<<endl;
}
void reverse2(linklist * head){
     linklist *p=head->next;
     linklist *last=NULL;
     linklist *temp;
     while (p!=NULL){
           temp=p->next;
           p->next=last;
           last=p;
           p=temp;
     }
     head->next=last;
}
int main(){
    datatype a[100]={1,2,3,4,5,6}; //Data for testing
    reverse1(a,6);
    for (int i =0;i<6;i++)
        cout<<a[i];
    cout<<endl;             //Test Reverse1
    
    linklist *head = new_linklist();    //Data for testing
    reverse2(head);
    print_linklist(head);  //Test Reverse2
  
    system("pause");
    return 0;
}

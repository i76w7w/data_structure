#include<iostream>
using namespace std;
const int MAX=100;
const int INF=0x7FFFFFFF;
void output (int A[MAX][MAX],int m,int n){
	for (int i=0;i<m;i++){
		for(int j=0;j<n;j++)
			cout<<A[i][j]<<" ";
		cout<<endl;
	}
}
void input(int A[MAX][MAX],int m,int n){
	for (int i=0;i<m;i++)
		for (int j=0;j<n;j++)
			cin>>A[i][j];
}
void find(int A[MAX][MAX],int m,int n){
	int q[MAX][MAX]={{0}};
	int i,j;
	for (i=0;i<m;i++){
		int min=INF;
		int c=0;
		for (j=0;j<n;j++)
			if (A[i][j]<min){
				min=A[i][j];
				c=j;
			}
		q[i][c]=1;
	}
/*	for (i=0;i<m;i++){
		for (j=0;j<n;j++)
			if(q[i][j]){
				cout<<i<<" "<<j<<endl;
				break;
			}
	}*/
	for (i=0;i<n;i++){
		int max=0;
		int c=0;
		for (j=0;j<m;j++)
			if (A[j][i]>max){
				max=A[j][i];
				c=j;
			}
		if(q[c][i])
			cout<<"Number:"<<max<<" "<<"Line:"<<c<<" "<<"Row:"<<i<<endl;
	}
}
int main(){
	int A[MAX][MAX]={{0}};
	int m,n;
	freopen("test.txt","r",stdin);    
	cin>>m>>n;
	output(A,m,n);
	input(A,m,n);
	output(A,m,n);
	find(A,m,n);
	return 0;
}
#include<iostream>
#define datatype char
using namespace std;
struct node{
	node *next;
	datatype data;
};
node *new_circular_linklist(){
	node *head=new node;
	node *p,*tail=head;
	datatype ch;
	head->data='H';
	while (1){
		p=new node;
		cin.get(ch);
		if (ch=='\n')
			break;
		p->data = ch;
		tail->next=p;
		tail=p;
	}
	tail->next=head;
	return head;
}
void print_circular_linklist(node *head){
	node *p=head;
	do{
		cout<<p->data;
		p=p->next;
	}while (p!=head);
	cout<<endl;
}
node *lookup(node *head,datatype data){
	node *p=head;
	do{
		if (p->data==data){
			return p;
		}else{
			p=p->next;
		}
	}while(p!=head);
}
void del(node *s){
	node *p=s;
	while (1){
		if (p->next->next==s){
			p->next=s;
			break;
		}else{
			p=p->next;
		}
	}
}
int main(){
	node *head=new_circular_linklist();
	print_circular_linklist(head->next->next);
	char ch;
	cout<<"Which number do you want to del?"<<endl;
	cin>>ch;
	node *p=lookup(head,ch);
	del(p);
	print_circular_linklist(p);
	return 0;
}
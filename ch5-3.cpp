#include<iostream>
const int MAXSIZE = 255;
using namespace std;
struct SeqString{
	char str[MAXSIZE];
	int len;
};
struct ListString{
	ListString *next;
	char ch;
};
ListString *New_ListString(){
	ListString *head=new ListString;
	ListString *tail=head;
	ListString *new_node;
	char ch;
	while (cin.get(ch)){
		if (ch!='\n'){
			new_node=new ListString;
			new_node->ch=ch;
			tail->next=new_node;
			tail=new_node;
		}else{
			tail->next=NULL;
			return head;
		}
	}
}
SeqString *New_SeqString(){
	SeqString *p=new SeqString;
	p->len=0;
	char ch;
	while (cin.get(ch)){
		if (ch == '\n'){
			p->str[p->len]=0;
			return p;
		}
		else
			p->str[(p->len)++]=ch;
	}
}
bool Equal_SeqString(SeqString*p1,SeqString*p2){
	if (p1->len != p2->len)
		return false;
	for(int i =0;i<p1->len;i++)
		if (p1->str[i]!=p2->str[i])
			return false;
	return true;
}
bool Equal_ListString(ListString*p1,ListString*p2){
	p1=p1->next;
	p2=p2->next;
	while ((p1!=NULL) && (p2!=NULL)){
		if ((p1->ch) != (p2->ch))
			return false;
		p1=p1->next;
		p2=p2->next;
	}
	if (p1==p2)
		return true;
	else
		return false;
}
int main(){
	SeqString *p1=New_SeqString();
	SeqString *p2=New_SeqString();
	if (Equal_SeqString(p1,p2))
		cout<<"True"<<endl;
	else 
		cout<<"False"<<endl;
	ListString *q1=New_ListString();
	ListString *q2=New_ListString();
	if (Equal_ListString(q1,q2))
		cout<<"True"<<endl;
	else
		cout<<"False"<<endl;

	return 0;
}
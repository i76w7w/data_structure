#include<iostream>
#include<cstring>
using namespace std;
const int MAXLEN=100;
void StrDelete (char *s,int i,int m){
	if (i>=strlen(s))
		return ;
	if (i+m>=strlen(s)){
		s[i-1]=0;
		return ;
	}
	strcpy(s+i,s+i+m);
	return ;
}
int main(){
	char s[MAXLEN];
	cin>>s;
	StrDelete(s,1,5);
	cout<<s<<endl;
	return 0;
}
#include<iostream>
#include<cstdio>
#include<malloc.h>
#define datatype char
using namespace std;
typedef struct node{
    datatype data;
    node *next;
}node;
node* new_linklist(){
    node* head= (node*)malloc(sizeof(node));
    node *p,*tail=head;
    datatype ch;
    while((ch=getchar())!='\n'){
        p=(node*)malloc(sizeof(node));
        tail->next=p;
        p->data=ch;
        tail=p;
    }
    tail->next=NULL;
    return head;
}
void print_linklist(node* head){
    node* p=head->next;
    while (p!=NULL){
        cout<<p->data;
        p=p->next;
    }
    cout<<endl;
}
node *merge(node* head1,node* head2){
    node *head3=(node*)malloc(sizeof(node));
    node *p1=head1->next,*p2=head2->next,*p3=head3;
    while (p1!=NULL && p2!=NULL){
        if (p1->data < p2->data){
            p3->next=p1;
            p1=p1->next;
        }else{
            p3->next=p2;
            p2=p2->next;
        }
        p3=p3->next;
    }
    if (p1==NULL)
        p3->next=p2;
    else  
        p3->next=p1;
    //reverse 
    p3=head3->next;
    node *last=NULL,*temp;
    while (p3!=NULL){
        temp=p3->next;
        p3->next=last;
        last=p3;
        p3=temp;
    }
    head3->next=last;
    return head3;
}
int main(){
    node *head1=new_linklist();
    node *head2=new_linklist();
    print_linklist(head1);
    print_linklist(head2);
    node *head3=merge(head1,head2);
    print_linklist(head3);
    return 0;
}

#include<iostream>
using namespace std;
const int MAXSIZE = 1000;
typedef char datatype ;
struct Stack{
	datatype data[MAXSIZE];
	int top;
};
Stack *new_Stack(){
	Stack *s=new Stack;
	s->top=0;
	return s ;
}
void push(Stack *s,datatype data){
	s->data[(s->top)++]=data;
}
datatype pop(Stack *s){
	return s->data[--(s->top)];
}
void del(Stack *s,Stack *temp,datatype data){
	datatype t;
	while (s->top > 0){
		t=pop(s);
		if(t!=data)
			push(temp,t);
	}
	while (temp->top > 0)
		push (s,pop(temp));
}
void print_Stack(Stack *s){
	int a=s->top -1 ;
	while (a>=0)
		cout<<s->data[a--];
}
int main(){
	Stack *s    = new_Stack();
	Stack *temp = new_Stack();
	datatype data;
	//Data for testing
	while (cin.get(data)){
		if (data!='\n'){
			push(s,data);
		}else{
			break;
		}
	}
	del(s,temp,'a');
	print_Stack(s);
	return 0;
}
/*
int main(){
	Stack *s=new_Stack();
	Linklist *head=new_Linklist();
	print_Linkist(head);
	reverse(head,s);
	print_Linkist(head);
	return 0;
}
*/
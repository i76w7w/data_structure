#include<iostream>
typedef char datatype;
const int MAXSIZE=1000;
using namespace std;
struct Stack
{
	datatype data[MAXSIZE];
	int top;
};
Stack *new_Stack(){
	Stack *s=new Stack;
	s->top=0;
	return s;
}
void push(Stack *s,datatype data){
	s->data[s->top]=data;
	(s->top)++;
}
datatype *pop(Stack *s){
	(s->top)--;
	datatype *ans = &(s->data[s->top]);
	return ans;
}
int input(datatype *str){
	datatype ch;
	int i=0;
	while (cin.get(ch)){
		if (ch=='\n'){
			str[i]=0;
			return i;
		}
		else
			str[i++]=ch;
	}
	str[i]=0;
	return i;
}
bool test(datatype *str,int len,Stack *s){
	int i;
	for (i=0;i<len/2;i++)
			push(s,str[i]);
	if (len%2) i++;
	for (;i<len;i++)
		if (*pop(s)!=str[i])
			return false;
	return true;
}
int main(){
	datatype str[MAXSIZE];
	int len;
	Stack *s;
	s=new_Stack();
	len=input(str);
	if (test(str,len,s))
		cout<<"Yes"<<endl;
	else
		cout<<"NO"<<endl;
	return 0;
}